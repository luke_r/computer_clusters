#!/bin/bash

# openmpi-2.1.1 on Gomora

name=openmpi
ver=2.1.1
base_dir=/opt/$name/$name-$ver
build_dir=$base_dir/bld_ifort
install_dir=${base_dir}_ifort
mkdir -p "$build_dir" "$install_dir"
cd "$build_dir"
../configure CC=icc CXX=icpc FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FCFLAGS="-O3 -xHost -i8" --prefix="$install_dir" --enable-mpirun-prefix-by-default --enable-static |& tee configure.log
make all install |& tee make.log
