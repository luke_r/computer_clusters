#!/bin/bash

# Molpro on Fermi

BDIR=$HOME/molpro2012
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
LAPACK_LIB="$BLAS_LIB"

cd "$BDIR"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /opt/ga/ga-git/build_intel -openmpi -var CFLAGS=-I/opt/openmpi/openmpi-current/include
alias mpirun="mpirun --allow-run-as-root"
