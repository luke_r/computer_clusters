#!/bin/bash

# Open MPI

name=openmpi
ver=3.0.1
compiler=intel

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install

mkdir -p "$bld_dir" "$inst_dir"
cd "$bld_dir"
# the "-i8" flag is needed with mpifort for mpi/mpi_f08 modules to work, so we use the "--with-wrapper-fcflags=-i8" option
common_flags="-O3 -xHost"
../configure \
	CC=icc CXX=icpc FC=ifort \
	CFLAGS="$common_flags" CXXFLAGS="$common_flags" FCFLAGS="$common_flags -i8" \
	--prefix="$inst_dir" --enable-mpirun-prefix-by-default \
	--enable-mpi-cxx --enable-sparse-groups \
	--with-wrapper-fcflags=-i8 \
	|& tee configure.log
make -j all |& tee make.log
make install |& tee -a make.log
