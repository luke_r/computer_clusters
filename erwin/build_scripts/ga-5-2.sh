#!/bin/bash

# ga-5-2 on Erwin

GDIR=/opt/ga
set -a
BLAS_I8=yes
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
mkdir -p "$GDIR/ga-5-2/bld_openmpi" "$GDIR/ga-5-2-install"
cd "$GDIR/ga-5-2/bld_openmpi"
../configure MPIF77=mpif77 MPICC=mpicc MPICXX=mpicxx CFLAGS="-O3 -xHost" CPPFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" F77_INT_FLAG=-i8 --prefix="$GDIR/ga-5-2-install" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB" 
make -j12
make checkprogs
make check MPIEXEC="mpirun -np 8"
make install
