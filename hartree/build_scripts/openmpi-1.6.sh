#!/bin/bash

# openmpi-1.6 on Hartree

ODIR=~rajchel3/openmpi
mkdir -p "$ODIR/openmpi-1.6-install"
cd "$ODIR/openmpi-1.6"
mkdir -p bld_intel && cd bld_intel && ../configure CC=icc CXX=icpc F77=ifort FC=ifort --prefix="$ODIR/openmpi-1.6-install"
make all install
