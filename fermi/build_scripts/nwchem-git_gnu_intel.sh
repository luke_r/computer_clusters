#!/bin/bash

# NWChem

name=nwchem
ver=git
compiler=gnu_intel

src_dir=/opt/$name/$name-$ver
inst_dir=${src_dir}-install_$compiler
rc_file=$inst_dir/data/default.nwchemrc

# environmental variables
set -a
NWCHEM_TOP=$src_dir
NWCHEM_TARGET=LINUX64
NWCHEM_MODULES=all
USE_MPI=y
ARMCI_NETWORK=MPI-TS
BLASOPT="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl"
LARGE_FILES=TRUE
USE_NOFSCHECK=TRUE
MRCC_METHODS=TRUE
USE_PYTHONCONFIG=Y
PYTHONHOME=/usr
PYTHONVERSION=2.6
FC=ifort

# memory setup
# how NWChem calculates memory:
# m = 128 * ((tm / KiB) / nCPU - mOS / KiB)
# tm - total memory
# nCPU - number of CPUs
# mOS = 30 * 2^10 KiB
# the flag to define memory: -DDFLT_TOT_MEM=m
# script to set memory: "$NWCHEM_TOP/contrib/getmem.nwchem"
LIB_DEFINES="-DDFLT_TOT_MEM=560571123"

# build NWChem
cd "$NWCHEM_TOP/src"
make nwchem_config |& tee make.log
make -j |& tee -a make.log
cd "$NWCHEM_TOP/src/util"
make version |& tee make.log
make -j |& tee -a make.log
cd "$NWCHEM_TOP/src"
make link |& tee -a make.log

# install NWChem for general site
rm -rf "$inst_dir/*"
mkdir -p "$inst_dir"/{bin,data}
cp "$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem" "$inst_dir/bin"
chmod 755 "$inst_dir/bin/nwchem"
cp -r "$NWCHEM_TOP/src/basis/libraries" "$inst_dir/data"
cp -r "$NWCHEM_TOP/src/data" "$inst_dir"
cp -r "$NWCHEM_TOP/src/nwpw/libraryps" "$inst_dir/data"
cd "$inst_dir/data"
find -type d -exec chmod o+rx '{}' \;
find -type f -exec chmod o+r '{}' \;

# create a global NWChem startup file
echo -n "\
nwchem_basis_library $inst_dir/data/libraries/
nwchem_nwpw_library $inst_dir/data/libraryps/
ffield amber
amber_1 $inst_dir/data/amber_s/
amber_2 $inst_dir/data/amber_q/
amber_3 $inst_dir/data/amber_x/
amber_4 $inst_dir/data/amber_u/
spce $inst_dir/data/solvents/spce.rst
charmm_s $inst_dir/data/charmm_s/
charmm_x $inst_dir/data/charmm_x/
" > "$rc_file"
ln -s "$rc_file" /opt/$name/.nwchemrc

# create links to the above file in home directories: "$HOME/.nwchemrc"
