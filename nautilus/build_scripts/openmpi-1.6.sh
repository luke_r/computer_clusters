#!/bin/bash

# openmpi-1.6 on Nautilus

ODIR=~lukarajc/openmpi
mkdir -p "$ODIR/openmpi-1.6-install"
mkdir -p "$ODIR/openmpi-1.6/bld_intel" && cd "$ODIR/openmpi-1.6/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort FFLAGS=-i8 FCFLAGS=-i8 --prefix="$ODIR/openmpi-1.6-install" --enable-static --with-openib --enable-mpirun-prefix-by-default --with-tm --with-mpi-f90-size=medium
make all install
