#!/bin/bash

# ga-5-1 on Nautilus

GDIR=~lukarajc/ga
set -a
BLAS_I8=yes
BLAS_LIB="-L/home/staff/lukarajc/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
mkdir -p "$GDIR/ga-5-1/bld_openmpi" "$GDIR/ga-5-1-install"
cd "$GDIR/ga-5-1/bld_openmpi"
../configure MPIF77=mpif77 MPICC=mpicc MPICXX=mpicxx --prefix="$GDIR/ga-5-1-install" --enable-cxx --enable-shared --with-blas="$BLAS_LIB"
make
make checkprogs
make check MPIEXEC="mpirun -np 4"
make install
