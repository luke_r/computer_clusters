# Dalton-2020

src_dir="$HOME/dalton/dalton-git"
inst_dir=/opt/dalton/dalton-2020
bld_dir="$src_dir/build-2020"
cd "$src_dir"
git checkout Dalton2020.1
git submodule update
git pull
git clean -dxff
./setup --prefix "$inst_dir" --mpi "$bld_dir"
cd "$bld_dir"
make -j && sudo make install
