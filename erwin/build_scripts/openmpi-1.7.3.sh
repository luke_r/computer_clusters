#!/bin/bash

# openmpi-1.7.3 on Erwin

ODIR=/opt/openmpi
mkdir -p "$ODIR/openmpi-1.7.3-install"
mkdir -p "$ODIR/openmpi-1.7.3/bld_intel" && cd "$ODIR/openmpi-1.7.3/bld_intel"
../configure CC=icc CXX=icpc FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FCFLAGS="-O3 -xHost -i8" --prefix="$ODIR/openmpi-1.7.3-install" --enable-mpirun-prefix-by-default --enable-static
make all install
