#!/bin/bash

# Open MPI

name=openmpi
ver=git
compiler=intel

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install_$compiler

mkdir -p "$bld_dir" "$inst_dir"
cd "$src_dir"
./autogen.pl
cd "$bld_dir"
# the "-i8" flag is needed with mpifort for mpi/mpi_f08 modules to work, so we use the "--with-wrapper-fcflags=-i8" option
common_intel_flags="-O3 -xHost"
../configure \
	CC=icc CXX=icpc FC=ifort \
	CFLAGS="$common_intel_flags" CXXFLAGS="$common_intel_flags" FCFLAGS="$common_intel_flags -i8" \
	--prefix="$inst_dir" --enable-mpirun-prefix-by-default \
	--enable-mpi-cxx --enable-sparse-groups \
	--with-wrapper-fcflags=-i8 \
	|& tee configure.log
make -j all |& tee make.log
make install |& tee -a make.log
