#!/bin/bash

# ga-5-1-1 on Goose

GDIR=/opt/ga
set -a
BLAS_I8=yes
BLAS_LIB="-L/opt/intel/mkl/lib -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
mkdir -p "$GDIR/ga-5-1-1/bld_openmpi" "$GDIR/ga-5-1-1-install"
cd "$GDIR/ga-5-1-1/bld_openmpi"
../configure MPIF77=mpif77 MPICC=mpicc MPICXX=mpicxx FFLAGS="-xHost -i8" FFLAG_INT=-i8 --prefix="$GDIR/ga-5-1-1-install" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB"
make
make checkprogs
make check MPIEXEC="mpirun -np 4"
make install
