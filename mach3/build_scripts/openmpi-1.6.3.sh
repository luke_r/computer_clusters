#!/bin/bash

# openmpi-1.6.3 on Mach3

ODIR=/opt/openmpi
mkdir -p "$ODIR/openmpi-1.6.3-install"
mkdir -p "$ODIR/openmpi-1.6.3/bld_intel" && cd "$ODIR/openmpi-1.6.3/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" FCFLAGS="-O3 -xHost -i8" --prefix="$ODIR/openmpi-1.6.3-install" --enable-static --with-mpi-f90-size=medium
make all install
