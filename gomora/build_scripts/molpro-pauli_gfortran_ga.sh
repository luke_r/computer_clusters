#!/bin/bash

# Molpro on Gomora

MDIR=/opt/molpro/molpro-pauli
BLAS_LIB="-L/opt/blas_int64_gcc/lib -lblas"
LAPACK_LIB="-L/opt/lapack-3.4.1_int64_gcc/lib -llapack"
set -a
PATH="$PATH:/opt/openmpi-1.6_int64_gcc/bin"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/openmpi-1.6_int64_gcc/lib:/opt/ga/ga-5-1-install/lib"

cd "$MDIR"
./configure -i8 -blas -lapack -mpp -mppbase /opt/ga/ga-5-1/bld_openmpi -openmpi -var CFLAGS=-I/opt/openmpi-1.6_int64_gcc/include -var LD_ENV=//opt/openmpi-1.6_int64_gcc/lib
