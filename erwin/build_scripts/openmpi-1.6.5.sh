#!/bin/bash

# openmpi-1.6.5 on Erwin

ODIR=/opt/openmpi
mkdir -p "$ODIR/openmpi-1.6.5-install"
mkdir -p "$ODIR/openmpi-1.6.5/bld_intel" && cd "$ODIR/openmpi-1.6.5/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" FCFLAGS="-O3 -xHost -i8" --prefix="$ODIR/openmpi-1.6.5-install" --enable-mpirun-prefix-by-default --enable-static --with-mpi-f90-size=medium
make all install
