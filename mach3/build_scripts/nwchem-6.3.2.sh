#!/bin/bash

# NWChem-6.3.2 on Mach3

# enviromental variables
set -a
LARGE_FILES=TRUE
USE_NOFSCHECK=TRUE
MRCC_THEORY=TRUE
NWCHEM_TOP=/opt/nwchem/nwchem-6.3.2-src
NWCHEM_INSTALL=/opt/nwchem/nwchem-6.3.2-install
NWCHEM_TARGET=LINUX64
NWCHEM_MODULES=all
USE_MPI=y
LIBMPI="-lmpi_f90 -lmpi_f77 -lmpi -lrt -lnsl -lutil -ldl -lm -Wl,--export-dynamic -lrt -lnsl -lutil"
MPI_LIB=/opt/openmpi/openmpi-current/lib
MPI_INCLUDE=/opt/openmpi/openmpi-current/include
ARMCI_NETWORK=SOCKETS
BLASOPT="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
LIB_DEFINES="-DDFLT_TOT_MEM=523178496"

# build NWChem
cd "$NWCHEM_TOP/src"
make FC=ifort CC=icc nwchem_config
make FC=ifort CC=icc >& make.log
cd "$NWCHEM_TOP/src/util"
make FC=ifort CC=icc version
make FC=ifort CC=icc
cd "$NWCHEM_TOP/src"
make FC=ifort CC=icc link

# memory setup
# how NWChem calculates memory:
# m = ((tm / kB) / nCPU - mOS / kB) * 128
# tm - total memory
# nCPU - number of CPUs
# mOS = 30 * 2^10 kB
# 1 kB = 2^10 B
# B = byte
# the flag to define memory: -DDFLT_TOT_MEM=m
# script to set memory: "$NWCHEM_TOP/contrib/getmem.nwchem"

# install NWChem for general site
mkdir -p "$NWCHEM_INSTALL"/{bin,data}
cp "$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem" "$NWCHEM_INSTALL/bin"
chmod 755 "$NWCHEM_INSTALL/bin/nwchem"
cp -r "$NWCHEM_TOP/src/basis/libraries" "$NWCHEM_INSTALL/data"
cp -r "$NWCHEM_TOP/src/data" "$NWCHEM_INSTALL"
cp -r "$NWCHEM_TOP/src/nwpw/libraryps" "$NWCHEM_INSTALL/data"
cd "$NWCHEM_INSTALL/data"
find -type d -exec chmod o+rx '{}' \;
find -type f -exec chmod o+r '{}' \;

# create a global NWChem startup file
echo -n "\
nwchem_basis_library $NWCHEM_INSTALL/data/libraries/
nwchem_nwpw_library $NWCHEM_INSTALL/data/libraryps/
ffield amber
amber_1 $NWCHEM_INSTALL/data/amber_s/
amber_2 $NWCHEM_INSTALL/data/amber_q/
amber_3 $NWCHEM_INSTALL/data/amber_x/
amber_4 $NWCHEM_INSTALL/data/amber_u/
spce $NWCHEM_INSTALL/data/solvents/spce.rst
charmm_s $NWCHEM_INSTALL/data/charmm_s/
charmm_x $NWCHEM_INSTALL/data/charmm_x/
" > "$NWCHEM_INSTALL/data/default.nwchemrc"

# create links to the above file in home directories: "$HOME/.nwchemrc"
