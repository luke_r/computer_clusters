#!/bin/bash

# openmpi-1.6.5 on Mach3

ODIR=/opt/openmpi
IDIR="$ODIR/openmpi-1.6.5-gfortran"
mkdir -p "$IDIR"
mkdir -p "$ODIR/openmpi-1.6.5/bld_gfortran" && cd "$ODIR/openmpi-1.6.5/bld_gfortran"
../configure CC=gcc CXX=g++ F77=gfortran FC=gfortran CFLAGS="-O3 -march=native" CXXFLAGS="-O3 -march=native" FFLAGS="-O3 -march=native -fdefault-integer-8" FCFLAGS="-O3 -march=native -fdefault-integer-8" --prefix="$IDIR" --enable-mpirun-prefix-by-default --enable-static --with-mpi-f90-size=medium
make all install
