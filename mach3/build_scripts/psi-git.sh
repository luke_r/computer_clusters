#!/bin/bash

# PSI

name=psi
ver=git
compiler=intel
export BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install

mkdir -p "$bld_dir" "$inst_dir"
cd "$src_dir"
common_flags="-O3 -xHost"
cmake -H. -B"$bld_dir" \
	-DCMAKE_C_COMPILER=icc \
	-DCMAKE_C_FLAGS="$common_flags" \
	-DCMAKE_CXX_COMPILER=icpc \
	-DCMAKE_CXX_FLAGS="$common_flags" \
	-DCMAKE_Fortran_COMPILER=ifort \
	-DCMAKE_Fortran_FLAGS="$common_flags -i8" \
	-DBLAS_TYPE=MKL \
	-DLAPACK_TYPE=MKL \
	-DCMAKE_INSTALL_PREFIX="$inst_dir" \
	|& tee cmake.log

cd "$bld_dir"
#make -j |& tee make.log
#make install |& tee -a make.log
