#!/bin/bash

# Molpro on Schrödinger

MDIR="$HOME/molpro-dev/molpro-pauli"
BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
GA_LDIR=/opt/ga/ga-current/lib

cd "$MDIR"
./configure FC=mpifort CXX=mpicxx --with-blas="$BLAS_LIB" --enable-mpp="$GA_LDIR" |& tee configure.log
