#!/bin/bash

# NWChem-6.1.1 on Hartree

# enviromental variables
set -a
LARGE_FILES=TRUE
USE_NOFSCHECK=TRUE
NWCHEM_TOP=~rajchel3/nwchem/nwchem-6.1.1-src
NWCHEM_INSTALL=~rajchel3/nwchem/nwchem-6.1.1-install
NWCHEM_TARGET=LINUX64
NWCHEM_MODULES=all
USE_MPI=y
USE_MPIF=y
MPI_LOC=~rajchel3/openmpi/openmpi-1.6.3-install
MPI_LIB="$MPI_LOC/lib"
MPI_INCLUDE="$MPI_LOC/include"
LIBMPI="-lmpi_f90 -lmpi_f77 -lmpi -lrt -lnsl -lutil -ldl -lm -Wl,--export-dynamic -lrt -lnsl -lutil"
BLASOPT="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"

# build NWChem
cd "$NWCHEM_TOP/src"
make FC=ifort CC=icc nwchem_config
make FC=ifort CC=icc >& make.log
cd "$NWCHEM_TOP/src/util"
make FC=ifort CC=icc version
make FC=ifort CC=icc
cd "$NWCHEM_TOP/src"
make FC=ifort CC=icc link

# memory setup
. /"$NWCHEM_TOP/contrib/getmem.nwchem"

# install NWChem for general site
mkdir -p "$NWCHEM_INSTALL"/{bin,data}
cp "$NWCHEM_TOP/bin/$NWCHEM_TARGET/nwchem" "$NWCHEM_INSTALL/bin"
chmod 755 "$NWCHEM_INSTALL/bin/nwchem"
cp -r "$NWCHEM_TOP/src/basis/libraries" "$NWCHEM_INSTALL/data"
cp -r "$NWCHEM_TOP/src/data" "$NWCHEM_INSTALL"
cp -r "$NWCHEM_TOP/src/nwpw/libraryps" "$NWCHEM_INSTALL/data"
