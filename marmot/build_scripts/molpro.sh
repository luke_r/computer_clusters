#!/bin/bash

# Molpro on Marmot

MDIR="$HOME/molpro-dev/molpro-sapt"
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
LAPACK_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"

cd "$MDIR"
./configure -blas -lapack -ifort -icc
