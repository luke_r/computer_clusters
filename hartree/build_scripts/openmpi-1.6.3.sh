#!/bin/bash

# openmpi-1.6.3 on Hartree

ODIR=~rajchel3/openmpi
mkdir -p "$ODIR/openmpi-1.6.3-install"
mkdir -p "$ODIR/openmpi-1.6.3/bld_intel" && cd "$ODIR/openmpi-1.6.3/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort FFLAGS=-i8 FCFLAGS=-i8 --prefix="$ODIR/openmpi-1.6.3-install" --enable-static --with-mpi-f90-size=medium
make all install
