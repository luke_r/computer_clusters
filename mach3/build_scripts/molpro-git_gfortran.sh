#!/bin/bash

# Molpro

MDIR="$HOME/molpro-dev/molpro-gfortran"
BLAS_LIB="-llapack64 -lblas64"
GA_LDIR=/usr/lib64/openmpi/lib

cd "$MDIR"
./configure FC=/usr/lib64/openmpi/bin/mpifort CXX=/usr/lib64/openmpi/bin/mpicxx --with-blas="$BLAS_LIB" --enable-mpp="$GA_LDIR"
