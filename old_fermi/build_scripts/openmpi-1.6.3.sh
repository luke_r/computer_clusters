#!/bin/bash

# openmpi-1.6.3 on Fermi

ODIR=/opt/openmpi
mkdir -p "$ODIR/openmpi-1.6.3-install"
mkdir -p "$ODIR/openmpi-1.6.3/bld_intel" && cd "$ODIR/openmpi-1.6.3/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort CFLAGS="-O3" CXXFLAGS="-O3" FFLAGS="-O3 -i8" FCFLAGS="-O3 -i8" --prefix="$ODIR/openmpi-1.6.3-install" --enable-static --with-mpi-f90-size=medium
make all install
