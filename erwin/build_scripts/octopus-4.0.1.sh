#!/bin/bash

# Octopus on Erwin

BDIR=/opt/octopus/octopus-4.0.1
IDIR=/opt/octopus/octopus-4.0.1-install
mkdir -p "$IDIR"
set -a
LIBS_BLAS="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lpthread -lm"
LIBS_LAPACK="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lpthread -lm"
LIBS_FFT="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -lpthread -lm"
FC="/opt/openmpi/openmpi-1.6.4-install/bin/mpif90"
FCFLAGS="-i8 -O3 -xHost -I$MKLROOT/include"
CC="/opt/openmpi/openmpi-1.6.4-install/bin/mpicc"
CFLAGS="-O3 -xHost -I$MKLROOT/include"
CPP="/opt/openmpi/openmpi-1.6.4-install/bin/mpicc -E"
CPPFLAGS="-I$MKLROOT/include"

autoreconf -i
cd "$BDIR"
./configure --prefix="$IDIR" --enable-mpi --enable-openmp --with-libxc-prefix=/opt/libxc/libxc-1.1.0-install --with-libxc-include=/opt/libxc/libxc-1.1.0-install/include --with-blas --with-lapack --with-fft-lib="$LIBS_FFT"
make
make check
make install
