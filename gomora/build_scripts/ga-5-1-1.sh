#!/bin/bash

# ga-5-1-1 on Gomora

GDIR=/opt/ga
set -a
BLAS_I8=yes
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
OEXEC=/opt/openmpi/openmpi-1.6.4_int64_ifort_wcppb/bin

mkdir -p "$GDIR/ga-5-1-1/bld_openmpi" "$GDIR/ga-5-1-1-install"
cd "$GDIR/ga-5-1-1/bld_openmpi"
../configure MPIF77="$OEXEC/mpif77" MPICC="$OEXEC/mpicc" MPICXX="$OEXEC/mpicxx" CFLAGS="-O3 -xHost" CPPFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" FFLAG_INT=-i8 --prefix="$GDIR/ga-5-1-1-install" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB"
make
make checkprogs
make check MPIEXEC="$OEXEC/mpirun -np 12"
make install
