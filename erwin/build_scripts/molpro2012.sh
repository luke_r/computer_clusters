#!/bin/bash

# Molpro on Erwin

MDIR="/opt/molpro/molpro2012"
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
LAPACK_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"

cd "$MDIR"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /opt/ga/ga-5-1-1/bld_openmpi -openmpi -var CFLAGS=-I/opt/openmpi/openmpi-current/include
