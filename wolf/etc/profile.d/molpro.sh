# Molpro
set -a
PATH=$PATH:/opt/molpro/molpro-2022.2/bin
MOLPRO_OPTIONS="-d /scratch/$LOGNAME/molpro -I /scratch/$LOGNAME/molpro -W /scratch/$LOGNAME/molpro/wfu --no-xml-output -s"
