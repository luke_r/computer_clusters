#!/bin/bash

# Molpro on Gomora

MDIR="$HOME/molpro-dev/molpro-pauli"
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
GA_BDIR=/opt/ga/ga-5-2/bld_openmpi
MPI_BIN=/opt/openmpi/openmpi-1.6.5-int64_ifort14/bin

set -a
PATH="$MPI_BIN:$PATH"

cd "$MDIR"
./configure FC="$MPI_BIN/mpif90" CXX="$MPI_BIN/mpicxx" --with-blas="$BLAS_LIB" --enable-mpp="$GA_BDIR"
