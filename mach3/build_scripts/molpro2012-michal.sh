#!/bin/bash

# Molpro 2012

src_dir=$HOME/molpro2012-michal
export BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"

cd "$src_dir"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /opt/ga/ga-git/build_intel -openmpi -var CFLAGS=-I/opt/openmpi/openmpi-current/include |& tee configure.log
