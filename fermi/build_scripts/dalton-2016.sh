#!/bin/bash

# Dalton

name=dalton
ver=2016
compiler=intel

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install

mkdir -p "$bld_dir" "$inst_dir"
cd "$src_dir"
./setup --mpi --mkl=sequential --int64 --prefix="$inst_dir" "$bld_dir" |& tee setup.log
cd "$bld_dir"
make -j |& tee make.log
make install |& tee -a make.log
