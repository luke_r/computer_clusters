#!/bin/bash

# openmpi-1.10.2 on Schrödinger

BNAME=openmpi
BDIR="/opt/$BNAME"
VER=1.10.2
mkdir -p "$BDIR/$BNAME-$VER-install"
mkdir -p "$BDIR/$BNAME-$VER/bld_intel" && cd "$BDIR/$BNAME-$VER/bld_intel"
../configure CC=icc CXX=icpc FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FCFLAGS="-O3 -xHost -i8" --prefix="$BDIR/$BNAME-$VER-install" --enable-mpirun-prefix-by-default --enable-static --enable-sparse-groups |& tee configure.log
make all install |& tee make.log
