#!/bin/bash

# ga-5-2 on Gomora

GDIR=/opt/ga
OEXEC=/opt/openmpi/openmpi-1.6.5-int64_ifort14/bin

set -a
BLAS_I8=yes
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
mkdir -p "$GDIR/ga-5-2/bld_openmpi" "$GDIR/ga-5-2-install"
cd "$GDIR/ga-5-2/bld_openmpi"
../configure MPIF77="$OEXEC/mpif77" MPICC="$OEXEC/mpicc" MPICXX="$OEXEC/mpicxx" CFLAGS="-O3 -xHost" CPPFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" F77_INT_FLAG=-i8 --prefix="$GDIR/ga-5-2-install" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB" 
make -j8
make checkprogs
make check MPIEXEC="$OEXEC/mpirun -np 8"
make install
