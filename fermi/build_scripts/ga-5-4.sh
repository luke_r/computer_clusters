#!/bin/bash

# ga-5-4 on Fermi

BDIR=/opt/ga
BVER=5-4
set -a
BLAS_I8=yes
BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
mkdir -p "$BDIR/ga-$BVER/bld_openmpi" "$BDIR/ga-$BVER-install"
cd "$BDIR/ga-$BVER/bld_openmpi"
../configure MPIF77=mpifort MPICC=mpicc MPICXX=mpicxx CFLAGS="-O3 -xHost" CPPFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" F77_INT_FLAG=-i8 --prefix="$BDIR/ga-$BVER-install" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB" 
make -j15
make checkprogs
make check MPIEXEC="mpirun --allow-run-as-root -np 5"
make install
