#!/bin/bash

# openmpi-1.6.5 on Gomora

ODIR=/opt/openmpi
ODIR_INSTALL="$ODIR/openmpi-1.6.5-int64_ifort14"
mkdir -p "$ODIR_INSTALL"
mkdir -p "$ODIR/openmpi-1.6.5/bld_intel" && cd "$ODIR/openmpi-1.6.5/bld_intel"
../configure CC=icc CXX=icpc F77=ifort FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FFLAGS="-O3 -xHost -i8" FCFLAGS="-O3 -xHost -i8" --prefix="$ODIR_INSTALL" --enable-mpirun-prefix-by-default --enable-static --with-mpi-f90-size=medium
make all install
