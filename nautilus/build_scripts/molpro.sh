#!/bin/bash

# Molpro on Nautilus

MDIR=~lukarajc/molpro-dev/molpro-pauli
export BLAS_LIB="-L/home/staff/lukarajc/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
cd "$MDIR"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /home/staff/lukarajc/ga/ga-5-1-1/bld_openmpi -openmpi -var CFLAGS=-I/home/staff/lukarajc/openmpi/current/include
