#!/bin/bash

# openmpi-1.8.4 on Fermi

BNAME=openmpi
BDIR="/opt/$BNAME"
VER=1.8.4
mkdir -p "$BDIR/$BNAME-$VER-install"
mkdir -p "$BDIR/$BNAME-$VER/bld_intel" && cd "$BDIR/$BNAME-$VER/bld_intel"
../configure CC=icc CXX=icpc FC=ifort CFLAGS="-O3 -xHost" CXXFLAGS="-O3 -xHost" FCFLAGS="-O3 -xHost -i8" --prefix="$BDIR/$BNAME-$VER-install" --enable-mpirun-prefix-by-default --enable-static
make all install
