#!/bin/bash

# libxc on Erwin

BDIR=/opt/libxc/libxc-1.1.0
IDIR=/opt/libxc/libxc-1.1.0-install
mkdir -p "$IDIR"
set -a
FC=ifort
FCFLAGS="-i8 -O3 -xHost"
FCCPP=fpp
CC=icc
CFLAGS="-O3 -xHost"
CPP="icc -E"

cd "$BDIR"
./configure --prefix="$IDIR" --enable-shared
make
make install
