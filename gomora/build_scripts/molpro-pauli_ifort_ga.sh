#!/bin/bash

# Molpro on Gomora

MDIR=/opt/molpro/molpro-pauli_ifort
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
LAPACK_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm"
set -a
PATH="/opt/openmpi-1.6_int64_ifort/bin:$PATH"

cd "$MDIR"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /opt/ga/ga-5-1-1/bld_openmpi_ifort -openmpi -var CFLAGS=-I/opt/openmpi-1.6_int64_ifort/include
