#!/bin/bash

# Global Arrays

name=ga
ver=git
compiler=intel
export BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install_$compiler

mkdir -p "$bld_dir" "$inst_dir"
cd "$src_dir"
./autogen.sh
# in case of network test timeout, try commenting out a section setting "FTP_OK=no" in "travis/install-autotools.sh"
cd "$bld_dir"
# setting "FCFLAGS" instead of "FFLAGS" doesn't work at the moment
# "-i8" flag is added automatically by mpifort compiled with "--with-wrapper-fcflags=-i8" Open MPI option, so we don't need to supply it
common_intel_flags="-O3 -xHost"
../configure \
	MPICC=mpicc MPICXX=mpicxx MPIFC=mpifort \
	CFLAGS="$common_intel_flags" CPPFLAGS="$common_intel_flags" CXXFLAGS="$common_intel_flags" FFLAGS="$common_intel_flags" \
	--prefix="$inst_dir" --enable-i8 --enable-cxx --enable-shared --with-blas="$BLAS_LIB" --with-lapack="$BLAS_LIB" \
	|& tee configure.log
make -j |& tee make.log
make checkprogs |& tee -a make.log
make check MPIEXEC="mpirun --allow-run-as-root -np 2" |& tee -a make.log
make install |& tee -a make.log
