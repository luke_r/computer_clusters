#!/bin/bash

# ABINIT

name=abinit
ver=git
compiler=intel
export BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"

src_dir=/opt/$name/$name-$ver
bld_dir=$src_dir/build_$compiler
inst_dir=${src_dir}-install

mkdir -p "$bld_dir" "$inst_dir"
cd "$src_dir"
*/*/makemake
cd "$bld_dir"
../configure \
	CC=mpicc CXX=mpicxx FC=mpifort \
	MPI_RUNNER="mpirun --allow-run-as-root" \
	--prefix="$inst_dir" --with-tardir="$bld_dir/tarballs" \
	--with-linalg-flavor=mkl --with-linalg-libs="$BLAS_LIB" \
	|& tee configure.log
make -j |& tee make.log
make install |& tee -a make.log
