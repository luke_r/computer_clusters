#!/bin/bash

# Molpro on Schrödinger

MDIR="/opt/molpro/molpro2012"
BLAS_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm" 
LAPACK_LIB="-L/opt/intel/mkl/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm" 

cd "$MDIR"
./configure -i8 -blas -lapack -ifort -icc -mpp -mppbase /opt/ga/ga-5-5/bld_openmpi -openmpi -var CFLAGS=-I/opt/openmpi/openmpi-current/include
