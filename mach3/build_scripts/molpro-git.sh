#!/bin/bash

# Molpro

src_dir=$HOME/molpro_dev/molpro-pauli
set -a
BLAS_LIB="-L$MKLROOT/lib/intel64 -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm"
GA_LIB_DIR=/opt/ga/ga-git-install/lib

cd "$src_dir"
./configure FC=mpifort CC=mpicc --with-blas="$BLAS_LIB" |& tee configure.log
# make -j |& make.log

# cd src/pauli
# make DOPT=gCONFIG
# make DOPT=gCONFIG

# GA and mpicxx don't work
# ./configure FC=mpifort CXX=mpicxx --with-blas="$BLAS_LIB" --enable-mpp="$GA_LIB_DIR" |& tee configure.log
